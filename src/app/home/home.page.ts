import { Component, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  genders= [];
  weight: number;
  gender: string;
  bottles: number;
  time: number;
  result: any;
  
  

  constructor() {}

  ngOnInit(){
    this.genders.push('Male');
    this.genders.push('Female');
    this.genders.push('Other');

    this.gender= 'Male';
    this.weight = 85;
    this.time = 13;
    this.bottles = 15;
    
  }

  calculate(){
    const litres = this.bottles * 0.33;
    const grams = litres * 8 * 4.5;
    const burning = this.weight / 10;
    const grams_left= grams - (burning * this.time);
    
    
    
    if (this.gender ==='Male'){
      this.result= grams_left/(this.weight *0.7);
    } else if (this.gender ==='Female') {
      this.result= grams_left/ (this.weight*0.6);
    } else {
      this.result= grams_left/ (this.weight*0.65);
    }
    
    

  }

}
